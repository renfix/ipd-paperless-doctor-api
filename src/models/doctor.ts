import { UUID } from 'crypto';
import knex, { Knex } from 'knex';

export class DoctorModel {
  constructor() { }

  /**
   * 
   * @param db 
   * @param userID
   * @returns 
   */
  getByUserID(db: Knex, userID: UUID) {
    return db('profile')
      .where('user_id', userID)
      .select(
        'id', 'user_id',
        'title', 'fname', 'lname',
        'license_no', 'dob', 'sex'
      )
      .first();
  }

  /**
   * 
   * @param db 
   * @param doctorID 
   * @returns 
   */
  getByUserIDs(db: Knex, userIDs: UUID[]) {
    return db('profile')
      .whereIn('user_id', userIDs)
      .select(
        'id', 'user_id',
        'title', 'fname', 'lname',
        'license_no', 'dob', 'sex'
      )
  }

}