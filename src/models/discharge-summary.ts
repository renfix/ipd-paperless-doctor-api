import { Knex } from 'knex';
import { UUID } from 'crypto';

export class DischargeSummaryModel {
  create(db: Knex, data: object) {
    return db('discharge_summary')
      .returning('id')
      .insert(data);
  }

  update(db: Knex, data: object, dischargeSummaryID: UUID) {
    return db('discharge_summary')
      .returning('id')
      .where('id', dischargeSummaryID)
      .update(data);
  }

  delete(db: Knex, dischargeSummaryID: UUID) {
    return db('discharge_summary')
      .returning('id')
      .where('id', dischargeSummaryID)
      .delete();
  }

  upsert(db: Knex, data: any) {
    return db('discharge_summary')
      .returning('id')
      .insert(data)
      .onConflict('id')
      .merge();
  }

  getByID(db: Knex, dischargeSummaryID: UUID) {
    return db('discharge_summary')
      .where('id', dischargeSummaryID)
      .andWhere('is_active', true)
      .select(
        'id', 'admit_id', 'diagnosis_by',
        'principle_diag', 'comobidity_diag',
        'complication_diag', 'other_diag',
        'external_cause_diag',
        'discharge_summary_date', 'discharge_summary_time'
      )
      .first();
  }

}