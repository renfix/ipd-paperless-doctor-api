import { UUID } from "crypto";
import { Knex } from "knex";

export class BedModel {
  constructor() { }

  getByID(db: Knex, bedID: UUID) {
    return db('bed')
    .where('id', bedID)
    .select(
      'id', 'name'
    )
    .first();
  }

  getByIDs(db: Knex, bedIDs: UUID[]) {
    return db('bed')
    .whereIn('id', bedIDs)
    .select(
      'id', 'name'
    )
  }

}