import { Knex } from 'knex';
import { UUID } from 'crypto';

export class StandingProgressNoteModel {

  constructor () { }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  create(db: Knex, data: any) {
    return db('standing_progress_note')
      .returning('id')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param data 
   * @param standingProgressNoteID 
   * @returns 
   */
  update(db: Knex, data: object, standingProgressNoteID: UUID) {
    return db('standing_progress_note')
      .returning('id')
      .where('id', standingProgressNoteID)
      .update(data)
  }

  /**
   * 
   * @param db 
   * @param standingProgressNoteID 
   * @returns 
   */
  delete(db: Knex, standingProgressNoteID: UUID) {
    return db('standing_progress_note')
      .returning('id')
      .where('id', standingProgressNoteID)
      .delete();
  }

  /**
   * 
   * @param db 
   * @param data 
   * @returns 
   */
  upsert(db: Knex, data: any) {
    return db('updating_progress_note')
      .insert(data)
      .onConflict('id')
      .merge();
  }

  /**
   * 
   * @param db 
   * @param groupDiseaseID 
   * @returns 
   */
  getByGroupDiseaseID(db: Knex, groupDiseaseID: UUID) {
    return db('standing_progress_note')
      .where('group_disease_id', groupDiseaseID)
      .andWhere('is_active', true)
      .select(
        'id', 'department_id',
        'progress_note_type_code', 'progress_note',
        'department_id'
      );
  }

  /**
   * 
   * @param db 
   * @param departmentID 
   * @returns 
   */
  getByDepartmentID(db: Knex, departmentID: UUID) {
    return db('standing_progress_note')
    .where('department_id', departmentID)
    .andWhere('is_active', true)
    .select(
      'id', 'department_id',
      'progress_note_type_code', 'progress_note',
      'department_id'
    );
  }

  getAll(db: Knex, search: any, limit: number, offset: number) {
    let sql = db('standing_progress_note')
    .select(
      'id', 'department_id',
      'progress_note_type_code', 'progress_note',
      'department_id'
    )
    
    if (search) {
      let query = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('progress_note_type_code like ?', [query])
          .orWhereRaw('progress_note like ?', [query])
      })
    }

    return sql.limit(limit).offset(offset);
  }

  getTotal(db: Knex, search: any) {
    let sql = db('standing_progress_note')

    if (search) {
      let query = `%${search}%`
      sql.where(builder => {
        builder.whereRaw('progress_note_type_code like ?', [query])
          .orWhereRaw('progress_note like ?', [query])
      })
    }

    return sql
      .count({ total: '*' }).first();
  }

  getList(db: Knex) {
    return db('standing_progress_note');
  }
  getGroupDiseaseByStandingProgressNote(db: Knex) {
    return db('l_group_disease').select(db.raw('l_group_disease.*')).innerJoin('standing_progress_note','standing_progress_note.group_disease_id','l_group_disease.id').groupBy(db.raw('l_group_disease.id'))
  }
};
