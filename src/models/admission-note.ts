import { Knex } from 'knex';
import { UUID } from 'crypto';

export class AdmissionNoteModel {

  create(db: Knex, data: any) {
    return db('admission_note')
    .insert(data)
    .returning(['id']);
  }
  
  update(db: Knex, data: any, id: UUID) {
    return db('admission_note')
    .where('id', id)
    .update(data);
  }
  
  updateByAdmitID(db: Knex, data: object, admitID: UUID) {
    return db('admission_note')
    .where('admit_id', admitID)
    .update(data);
    
  }

  delete(db: Knex, id: UUID) {
    return db('admission_note')
      .where('id', id)
      .delete();
  }

  deleteByAdmitID(db: Knex, admitID: UUID) {
    return db('admission_note')
      .where('admit_id', admitID)
      .delete();
  }

  upsert(db: Knex, data: any) {
    return db('admission_note')
      .insert(data)
      .onConflict('admit_id')
      .merge()
      .returning('*');
  }

  getByAdmitID(db: Knex, admitID: UUID) {
    return db('admission_note')
    .where('admit_id', admitID)
    .andWhere('is_active', true)
    // .select(
    //   'id', 'admit_id', 'pre_diagnosis', 'plan_of_treatment', 'review_of_system', 'physical_examination'
    // )
  }

}