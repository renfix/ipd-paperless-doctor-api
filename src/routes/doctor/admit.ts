import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { UUID } from 'crypto';

import { AdmitModel } from '../../models/admit';
import { DepartmentModel } from '../../models/department';
import { PatientModel } from '../../models/patient';
import { WardModel } from '../../models/ward';
import { BedModel } from '../../models/bed';
import { DoctorModel } from '../../models/doctor';
import { InsuranceModel } from '../../models/insurance';
import { OpdReviewModel } from '../../models/opd-review';
import { PatientAllergyModel } from '../../models/patient-allergy';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admitModel = new AdmitModel();
  const patientModel = new PatientModel();
  const departmentModel = new DepartmentModel();
  const wardModel = new WardModel();
  const bedModel = new BedModel();
  const doctorModel = new DoctorModel();
  const insuranceModel = new InsuranceModel();
  const opdReviewModel = new OpdReviewModel();
  const patientAllergyModel = new PatientAllergyModel();

  // get patient admit by doctor id (admit,ward,bed)
  // /doctor/admit/:doctor_id/patient-doctor
  fastify.get('/:doctor_id/patient-doctor', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const doctorID: any = params.doctor_id

    try {
      const data: any = await admitModel.getByDoctorID(db, doctorID)
      reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data
        })
    } catch (error) {
      reply.status(StatusCodes.OK)
        .send({
          ok: false,
          error
        })
    }

  })

  // get patient info by admit id (admit,patient,opd_review,opd_treatment)
  // /doctor/admit/:admit_id/patient-info
  fastify.get('/:admit_id/patient-info', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const params: any = request.params;
      const admitID: any = params.admit_id;
      var data = {};
      var admit: any = {};
      var patient: any = {};
      var department: any = {};
      var ward: any = {};
      var bed: any = {};
      var doctor: any = {};
      var insurance: any = {};
      var opdReview: any = {};
      var patientAllergy: any = {};

      admit = await admitModel.getByID(db, admitID)
      if(admit) {
        patient = await patientModel.getByAdmitID(db, admitID);
        department = await departmentModel.getByID(db, admit.department_id);
        ward = await wardModel.getByID(db, admit.ward_id);
        bed = await bedModel.getByID(db, admit.bed_id);
        doctor = await doctorModel.getByUserID(db, admit.doctor_id);
        insurance = await insuranceModel.getByID(db, admit.insurance_id);
        opdReview = await opdReviewModel.getByAdmitID(db, admit.id);
        patientAllergy = await patientAllergyModel.getByAdmitID(db, admit.id);
      }
      data = {
        ...admit,
        patient,
        department,
        ward,
        bed,
        doctor,
        insurance,
        opd_review: opdReview,
        patient_allergy: patientAllergy
      }

      return reply.status(StatusCodes.OK).send({
        ok: true,
        data
      })
    } catch (error) {
      console.log(error)
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        })
    }
  })

  // get patient admit in ward
  // /doctor/admit/:ward_id/patient-ward
  fastify.get('/:ward_id/patient-ward', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const params: any = request.params
      const query: any = request.query
      const wardID: any = params.ward_id
      const search: any = query.search
      const limit: any = query.limit
      const offset: any = query.offset
      try {
        const resTotal: any = await admitModel.getTotalAdmitWard(db, wardID, search)
        const results: any = await admitModel.getAdmitByWardID(db, wardID, search, limit, offset)
        return reply.status(StatusCodes.OK)
          .send({
            ok: true,
            data: results,
            total: Number(resTotal[0].count)
          });
      } catch (error) {
        return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
          .send({
            ok: false,
            error
          })
      }

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  done();
}