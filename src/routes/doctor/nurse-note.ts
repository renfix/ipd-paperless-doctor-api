import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';
import { UUID } from 'crypto';
import { NurseNoteModel } from '../../models/nures-note';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const nurseNoteModel = new NurseNoteModel();

  // get view nurse note by admit id
  // /doctor/nurse-note/:admit_id
  fastify.get('/:admit_id', {
    preHandler: [
      fastify.guard.role( 'admin', 'nurse', 'doctor'),
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params
    const admitID: UUID = params.admit_id;
    
    try {
      const data = await nurseNoteModel.getByAdmitID(db, admitID);

      return reply.status(StatusCodes.OK).
        send({
          ok: true,
          data
        });
    } catch (error: any) {
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          ok: false,
          error
        });
    }
  })

  done();
}